function renderEmptyValidator() {
  return '<small class="error js-error">This field is empty</small>';
}

function appendError(name) {
  $(`input[name="${name}"]`)
    .parent()
    .append(renderEmptyValidator());
}

$(document).ready(function() {
  $('.js-signin-form').on('submit', function(e) {
    e.preventDefault();

    const emailValue = document.forms['signin-form']['email'].value;
    const passValue = document.forms['signin-form']['password'].value;
    $('.js-error').remove();
    if (!emailValue) {
      appendError('email');
    }
    if (!passValue) {
      appendError('password');
    }
  });
});
