const TAB_DATA = [
  {
    id: 'tab-1',
    title: 'Tab 1',
    text: 'This data for tab 1'
  },
  {
    id: 'tab-2',
    title: 'Tab 2',
    text: 'This data for tab 2'
  },
  {
    id: 'tab-3',
    title: 'Tab 3',
    text: 'This data for tab 3'
  }
];

function renderTabItem(tabItem, index) {
  return `<a role="button" data-toggle="tab" id="${
    tabItem.id
  }" class="tab-item js-tab-item">${tabItem.title}</a>`;
}

function renderTabs() {
  TAB_DATA.map(item => {
    $('.js-tab').append(renderTabItem(item));
  });
}

function renderTabContentItem(tabItem) {
  return `<div id="${
    tabItem.id
  }" class="tab-content-item js-tab-content-item"><p>${tabItem.text}</p></div>`;
}

function renderTabContent() {
  TAB_DATA.map(item => {
    $('.js-tab-content').append(renderTabContentItem(item));
  });
}

function addActiveTab(className) {
  $(className).addClass('active');
}

function removeActiveTab(className) {
  $(className).removeClass('active');
}

function addActiveTabWithId(id) {
  removeActiveTab('.js-tab > .active');
  removeActiveTab('.js-tab-content > .active');
  addActiveTab(`.js-tab > #${id}`);
  addActiveTab(`.js-tab-content > #${id}`);
}

function openTab(id) {
  $(document).ready(function() {
    addActiveTabWithId(id);
  });
}

$(document).ready(function() {
  renderTabs();
  renderTabContent();
  addActiveTabWithId(TAB_DATA[0].id);
  $('.js-tab-item').click(function() {
    openTab($(this).attr('id'));
  });
});
