"use strict";

var MENU_LIST = [{
  name: 'Menu 1',
  id: '1'
}, {
  name: 'Menu 2',
  id: '2'
}, {
  id: '3',
  name: 'Menu 3',
  subMenu: [{
    id: '3-1',
    name: 'Submenu 1'
  }, {
    id: '3-2',
    name: 'Submenu 2'
  }, {
    id: '3-3',
    name: 'Submenu 3'
  }]
}];

function renderMenuItem(menuItem, index) {
  return "\n <a href=\"#\" class=\"menu-item js-menu-item-".concat(index, "\">\n      ").concat(menuItem.name, " ").concat(menuItem.subMenu ? '<i class="fas fa-caret-down"></i>' : '', "\n    </a>\n  ");
}

function renderMenu(menu, className) {
  menu.map(function (item, index) {
    $(className).append(renderMenuItem(item, index));

    if (item.subMenu) {
      $(".js-menu-item-".concat(index)).append('<div class="dropdown bordered"></div>');
      renderMenu(item.subMenu, ".js-menu-item-".concat(index, " > .dropdown"));
    }
  });
}

$(document).ready(function () {
  renderMenu(MENU_LIST, '.js-menu');
});
//# sourceMappingURL=menu.js.map
