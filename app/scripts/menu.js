const MENU_LIST = [
  { name: 'Menu 1', id: '1' },
  { name: 'Menu 2', id: '2' },
  {
    id: '3',
    name: 'Menu 3',
    subMenu: [
      { id: '3-1', name: 'Submenu 1' },
      { id: '3-2', name: 'Submenu 2' },
      { id: '3-3', name: 'Submenu 3' }
    ]
  }
];

function renderMenuItem(menuItem, index, isMobile) {
  const classSubmenu = isMobile ? 'sub-menu-view' : 'dropdown';
  const dropdownClass = menuItem.subMenu ? classSubmenu : '';
  const arrow = isMobile ? 'right' : 'down';
  const caretIcon = menuItem.subMenu
    ? `<i class="fa fa-caret-${arrow}"></i>`
    : '';
  return `
 <a href="#" class="menu-item js-menu-item-${index} ${dropdownClass}">
      ${menuItem.name} ${caretIcon}
    </a>
  `;
}

function renderDesktopMenu(menu, className) {
  menu.map((item, index) => {
    $(className).append(renderMenuItem(item, index, false));
    if (item.subMenu) {
      $(`.js-menu-item-${index}`).append(
        '<div class="sub-menu dropdown-content bordered"></div>'
      );
      renderDesktopMenu(
        item.subMenu,
        `.js-menu-item-${index} > .dropdown-content`
      );
    }
  });
}

function renderMobileMenu(menu, className) {
  menu.map((item, index) => {
    $(className).append(renderMenuItem(item, index, true));
    if (item.subMenu) {
      $(`.js-menu-item-${index}`).append(
        `<div class="sub-menu-view-items">
            <a href="#" role="button" class="menu-item close-sub-button">
              <div><i class="fa fa-caret-left"></i>&nbsp;&nbsp;Back</div>   
            </a>
            <div class="menu-item bg-black active-menu">${item.name}</div>
        </div>`
      );
      renderMobileMenu(
        item.subMenu,
        `.js-menu-item-${index} > .sub-menu-view-items`
      );
    }
  });
}

$(document).ready(function() {
  renderDesktopMenu(MENU_LIST, '.js-desktop-view > .js-menu');
  renderMobileMenu(MENU_LIST, '.js-mobile-view > .js-menu');
  $('.js-menu-overlay').hide();
});
