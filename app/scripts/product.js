const productItem = {
  image: './images/auth-image-default.jpg',
  name: 'RESORT STYLE',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor'
};

const setUpProduct = () => {
  var products = [];
  for (var i = 0; i <= 19; i++) {
    products.push(productItem);
  }
  return products;
};

function renderProductItem(item) {
  return `<div class="product-item">
          <div class="product-content">
            <div class="image" style="background-image:url(${
              item.image
            })"></div>
              <div class="information">
              <div class="bold uppercase">${item.name}</div>
              <div class="bold description"><p>${item.description}</p></div>
          </div>
        </div> 
        </div>
    `;
}

$(document).ready(function() {
  setUpProduct().map(item => {
    $('.js-products').append(renderProductItem(item));
  });
});
