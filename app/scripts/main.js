function closeModal() {
  $('.modal').css('display', 'none');
}

function closeNav() {
  $(document).ready(function() {
    $('.js-mobile-view > .js-menu').css('width', '0px');
    $('.js-mobile-view').css('margin-left', '0px');
    $('.js-content').css('margin-left', '0px');
  });
}

function toggleNav(isOpen) {
  const width = isOpen ? '215px' : '0px';
  $(document).ready(function() {
    $('.js-mobile-view > .js-menu').css('width', width);
    $('.js-mobile-view').css('margin-left', width);
    $('.js-content').css('margin-left', width);
    if (isOpen) {
      $('.js-menu-overlay').show();
    } else {
      $('.js-menu-overlay').hide();
    }
  });
}

$(document).ready(function() {
  window.onclick = function(event) {
    const className = event.target.className;
    const menuEl = $('.js-menu-overlay');
    if (className === menuEl.attr('class')) {
      toggleNav(false);
    }
    if (className === $('.modal').attr('class')) {
      closeModal();
    }
  };
});
